﻿using System;
using GeometryLibrary;
using GeometryLibrary.Types;

namespace App
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Figure figureA = new Triangle(3, 4, 5);
            Console.WriteLine($"Area: {figureA.FindArea()}");

            Figure figureB = new Circle(5);
            Console.WriteLine($"Area: {figureB.FindArea()}");
        }
    }
}