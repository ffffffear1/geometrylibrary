﻿using GeometryLibrary.Types;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void TestFindAreaRectangularTriangle()
        {
            Triangle triangle = new Triangle(3,4,5);
            Assert.Positive(triangle.isRectangular.Equals(true) ? 1 : 0);
        }
        
        [Test]
        public void TestFindAreaTriangle()
        {
            Triangle triangle = new Triangle(6,5,1);
            Assert.Positive(triangle.area >= 0 ? 1 : 0);
        }
        
        [Test]
        public void TestFindAreaCircle()
        {
            Circle circle = new Circle(6);
            Assert.Positive(circle.area >= 0 ? 1 : 0);
        }
    }
}