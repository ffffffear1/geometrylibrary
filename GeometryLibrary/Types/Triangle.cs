﻿using System;

namespace GeometryLibrary.Types
{
    public class Triangle : Figure
    {
        private readonly double halfMeter;
        public readonly double area;
        public readonly bool isRectangular;
        
        public Triangle(double aSide, double bSide, double cSide)
        {
            isRectangular = IsRectangular(aSide, bSide, cSide);
            halfMeter = (aSide + bSide + cSide) / 2;
            area = Math.Sqrt(halfMeter * (halfMeter - aSide) * (halfMeter - bSide) * (halfMeter - cSide));
        }

        public override double FindArea()
        {
            if (double.IsNaN(area))
                Console.Write("There can be no such triangle 0_");
            else
            {
                if(isRectangular) Console.WriteLine("Rectangular triangle");
                return area;
            }

            return 0;
        }

        private static bool IsRectangular(double aSide, double bSide, double cSide)
        {
            return Math.Pow(cSide, 2) == Math.Pow(aSide, 2) + Math.Pow(bSide, 2);
        }
    }
}