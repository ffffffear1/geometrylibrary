﻿using System;

namespace GeometryLibrary.Types
{
    public class Circle : Figure
    {
        public readonly double area;

        public Circle(double radius) => area = Math.PI * Math.Pow(radius, 2);
        
        public override double FindArea()
        {
            return area;
        }
    }
}