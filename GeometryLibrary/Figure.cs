﻿namespace GeometryLibrary
{
    public abstract class Figure
    {
        public abstract double FindArea();
    }
}